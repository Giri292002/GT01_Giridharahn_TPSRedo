using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.RemoteConfig;

public class ZombieRemoteConfig : RemoteConfig_Abstract
{
    [SerializeField]
    private AIController _aic;

    private float _zombieMaxDamage;
    private float _zombieMaxHealth;
    private float _zombieTimeBetweenAttacks;

    protected override
    void ApplyRemoteSettings(ConfigResponse configResponse)
    {
        switch (configResponse.requestOrigin)
        {
            case ConfigOrigin.Default:
                Debug.Log("No settings loaded this session; using default values.");
                break;
            case ConfigOrigin.Cached:
                Debug.Log("Using cached values");
                break;
            case ConfigOrigin.Remote:
                Debug.Log("Got New Settings woohoo");
                _zombieMaxDamage = ConfigManager.appConfig.GetFloat("zombie_MaxDamage");
                _zombieMaxHealth = ConfigManager.appConfig.GetFloat("zombie_MaxHealth");
                _zombieTimeBetweenAttacks = ConfigManager.appConfig.GetFloat("zombie_TimeBetweenAttacks");
                ApplyValues();
                break;
        }
    }

    protected override
    void ApplyValues()
    {
        _aic.Damage = _zombieMaxDamage;
        _aic.MaxHealth = _zombieMaxHealth;
        _aic.TimeBetweenAttacks = _zombieTimeBetweenAttacks;
        Debug.Log($"Damage Config : {_aic.Damage} MaxHealth Config : {_aic.MaxHealth} TBA Config: {_aic.TimeBetweenAttacks}");
    }
}
