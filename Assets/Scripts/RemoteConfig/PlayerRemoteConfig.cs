﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Unity.RemoteConfig;

public class PlayerRemoteConfig : RemoteConfig_Abstract
{
    [SerializeField]
    private CharacterController _cc;

    private float _playerMaxHealth;
    private int _playerMaxShootCount;


    protected override
    void ApplyRemoteSettings(ConfigResponse configResponse)
    {
        switch (configResponse.requestOrigin)
        {
            case ConfigOrigin.Default:
                Debug.Log("No settings loaded this session; using default values.");
                break;
            case ConfigOrigin.Cached:
                Debug.Log("Using cached values");
                break;
            case ConfigOrigin.Remote:
                Debug.Log("Got New Settings woohoo");

                _playerMaxHealth = ConfigManager.appConfig.GetFloat("player_MaxHealth");
                _playerMaxShootCount = ConfigManager.appConfig.GetInt("player_MaxShootCount");

                ApplyValues();
                break;
        }
    }

    protected override
    void ApplyValues()
    {
        _cc.MaxHealth = _playerMaxHealth;
        _cc.MaxShootCount = _playerMaxShootCount;
        Debug.Log($"Player Shoot Count: {_playerMaxShootCount} and Max Health: {_playerMaxHealth}");
    }
}
