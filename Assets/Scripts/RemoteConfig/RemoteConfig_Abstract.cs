﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.RemoteConfig;

public class RemoteConfig_Abstract : MonoBehaviour
{
    public struct UserAttributes { }
    public struct AppAttributes { }

    private UserAttributes _userAttributes;
    private AppAttributes _appAttributes;


    private void Awake()
    {
        _userAttributes = new UserAttributes();
        _appAttributes = new AppAttributes();
        FetchNow();
    }

    public void FetchNow()
    {
        ConfigManager.FetchConfigs(_userAttributes, _appAttributes);
    }

    protected void Start()
    {
        ConfigManager.FetchCompleted += ApplyRemoteSettings;
    }

    virtual
    protected void ApplyRemoteSettings(ConfigResponse configResponse)
    {
        switch (configResponse.requestOrigin)
        {
            case ConfigOrigin.Default:
                Debug.Log("No settings loaded this session; using default values.");
                break;
            case ConfigOrigin.Cached:
                Debug.Log("Using cached values");
                break;
            case ConfigOrigin.Remote:
                ApplyValues();
                break;
        }
    }

    virtual
    protected void ApplyValues()
    {
        Debug.Log("Applied Values");
    }
}
