﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Attacking : AI_Parent
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _aic = animator.GetComponentInParent<AIController>();
        _aic.NavAgent.isStopped = true;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (_aic.CurrentTarget != null && _aic.CurrentTarget.IsAlive)
        {
            _aic.AttackTimer += Time.deltaTime;
            var lookPos = _aic.CurrentTarget.transform.position.With(y: _aic.transform.position.y);
            _aic.transform.LookAt(lookPos);
            if (_aic.AttackTimer >= _aic.TimeBetweenAttacks)
            {
                _aic.ShootLasersFromEyes(_aic.CurrentTarget.transform.position + Vector3.up, _aic.CurrentTarget);
                _aic.AttackTimer -= _aic.TimeBetweenAttacks;
            }
        }
        else
        {
            _aic.CurrentTarget = null;
            animator.SetTrigger("idle");
        }


    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
