﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_MovingToOutpost : AI_Parent
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _aic = animator.GetComponentInParent<AIController>();
        _aic.NavAgent.SetDestination(_aic.CurrentOutpost.transform.position);
        _aic.NavAgent.isStopped = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        if (_aic._reached == false)
        {
            _aic.SetEnemyUnitAroundMe();
        }
        if (_aic._reached == true)
        {
            animator.SetTrigger("captureOutpost");
        }
    }
}
