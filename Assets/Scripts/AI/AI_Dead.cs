﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Dead : AI_Parent
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _aic = animator.GetComponentInParent<AIController>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _aic.CurrentTarget = null;
        _aic.CurrentOutpost = null;
        if (_aic.NavAgent.isActiveAndEnabled)
        {
            _aic.NavAgent.isStopped = true;
            _aic.NavAgent?.ResetPath();
            _aic.NavAgent.enabled = false;
        }
        var col = _aic.GetComponent<Collider>();
        if (col != null) Destroy(col);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
