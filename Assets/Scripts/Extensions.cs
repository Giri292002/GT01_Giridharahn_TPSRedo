using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static Vector3 With(this Vector3 original, float? x = null, float? y = null, float? z = null)
    {
        return new Vector3(x ?? original.x, y ?? original.y, z ?? original.z);
    }

    public static Outpost GetClosestTo(this List<Outpost> list, Vector3 pos, Outpost exclude = null)
    {
        if (list == null || list.Count == 0f) return null;

        Outpost closestO = null;
        float closestD = float.MaxValue;
        foreach (var item in list)
        {
            if (item == exclude) continue;

            var dist = Vector3.Distance(item.transform.position, pos);
            if (dist < closestD)
            {
                closestD = dist;
                closestO = item;
            }
        }
        return closestO;
    }

}
