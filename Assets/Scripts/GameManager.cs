using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int Winner { get; private set; } = -1;

    [SerializeField]
    private float _gameOverTimer = 5f;

    // Singleton
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            SingletonAwake();
        }
        else
        {
            Destroy(this);
        }
    }

    public Color[] TeamColors;
    private float _winTime = 0;

    public int[] TeamScores { get; private set; }

    protected void SingletonAwake()
    {
        TeamScores = new int[TeamColors.Length];
    }

    protected void Update()
    {
        ResetTeamScores();
        UpdateTeamScores();
        CheckGameOver();
        GameOverTime();
    }

    private void GameOverTime()
    {
        if (Winner != -1)
        {
            if (Time.time - _winTime >= _gameOverTimer)
            {
                UIScreenController.ShowScreen<MainMenuUIScreen>();
                SceneManager.LoadScene("MainMenuScene");
            }
        }
    }

    private void CheckGameOver()
    {
        if (Winner != -1) return;

        for (int i = 0; i < TeamScores.Length; i++)
        {
            if (TeamScores[i] == Outpost.OutpostList.Count)
            {
                Winner = i;
                _winTime = Time.time;
                UIScreenController.ShowScreen<GameOverUIScreen>();
                return;
            }
        }
    }

    private void UpdateTeamScores()
    {
        foreach (var item in Outpost.OutpostList)
        {
            if (item.CaptureValue >= 1f)
            {
                TeamScores[item.CurrentTeam]++;
            }
        }
    }

    private void ResetTeamScores()
    {
        for (int i = 0; i < TeamScores.Length; i++)
        {
            TeamScores[i] = 0;
        }
    }
}
