﻿using UnityEngine;

public class LifeSpan : MonoBehaviour
{
    [Tooltip("<0 means infinite time")]
    [SerializeField]
    private float _lifeSpan = -1;

    private void Awake()
    {
        if (_lifeSpan < 0f) return;
        Destroy(gameObject, _lifeSpan);
    }
}
