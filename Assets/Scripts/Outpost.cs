﻿using System.Collections.Generic;
using UnityEngine;
using PoolSystem;

public class Outpost : MonoBehaviour
{
    public static List<Outpost> OutpostList { get; private set; } = new List<Outpost>();

    [SerializeField]
    private float _captureTime = 5f;

    [SerializeField]
    private Renderer _flagRenderer = null;

    [SerializeField]
    private PoolableObject _zombiePrefab = null;

    [SerializeField]
    private float _spawnTimer = 3f;

    public float CaptureValue { get; private set; } = 0f;
    public int CurrentTeam { get; private set; } = 0;
    private float _lastTimeSpawn = 0f;


    private void Awake()
    {
        OutpostList.Add(this);
    }

    private void OnDestroy()
    {
        OutpostList.Remove(this);
    }


    // gets called Every Physics Tick (Fixed update)
    // per Collider inside the Trigger Volume
    private void OnTriggerStay(Collider other)
    {
        var otherUnit = other.GetComponent<Unit>();
        if (otherUnit == null || otherUnit.IsAlive == false) return;

        if (otherUnit.TeamNumber == CurrentTeam)
        {
            CaptureValue += Time.deltaTime / _captureTime;
            if (CaptureValue > 1f) CaptureValue = 1f;
        }
        else
        {
            CaptureValue -= Time.deltaTime / _captureTime;
            if (CaptureValue <= 0f)
            {
                CaptureValue = 0f;
                CurrentTeam = otherUnit.TeamNumber;
            }
        }
    }

    private void Update()
    {
        UpdateFlagColor();
        TrySpawnUnitsForTeam();
    }

    private void TrySpawnUnitsForTeam()
    {
        if (CaptureValue >= 1f)
        {
            // spawn a clone of a unit every X seconds
            if (Time.time - _lastTimeSpawn >= _spawnTimer)
            {
                _lastTimeSpawn = Time.time;
                PoolManager.GetNext(_zombiePrefab, transform.position, transform.rotation).GetComponent<AIController>().SetTeam(CurrentTeam);
            }
        }
        else
        {
            _lastTimeSpawn = Time.time;
        }
    }

    private void UpdateFlagColor()
    {
        if (_flagRenderer == null) return;

        var mat = _flagRenderer.material;
        var col = GameManager.Instance.TeamColors[CurrentTeam];
        mat.color = Color.Lerp(Color.white, col, CaptureValue);
    }

    public static Outpost GetRandom()
    {
        if (OutpostList == null || OutpostList.Count == 0) return null;

        int r = Random.Range(0, OutpostList.Count);
        return OutpostList[r];
    }
}
