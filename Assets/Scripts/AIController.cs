using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using PoolSystem;


[RequireComponent(typeof(NavMeshAgent))]
public class AIController : Unit
{

    [SerializeField]
    private float _enemyDetectionRadius = 8f;

    [SerializeField]
    public float TimeBetweenAttacks = 1f;

    [SerializeField]
    private LayerMask _unitLayerMask;

    [SerializeField]
    private Image _healthImage;
    private Canvas _healthCanvas;


    public NavMeshAgent NavAgent = null;
    public Outpost CurrentOutpost = null;
    public Unit CurrentTarget = null;
    public float AttackTimer = 0f;
    public bool _reached;

    override
    protected void UnitAwake()
    {
        NavAgent = GetComponent<NavMeshAgent>();
        _healthCanvas = _healthImage.GetComponentInParent<Canvas>();
        _healthImage.fillAmount = Mathf.InverseLerp(0, MaxHealth, _currentHealth);
    }

    override
    protected void UnitStart()
    {

    }
    private void Update()
    {
        if (IsAlive == false) return;
        ReachedOutpost();
    }

    public void ReachedOutpost()
    {
        if (CurrentOutpost == null)
        {
            _reached = false;
            return;
        }
        if (NavAgent.isActiveAndEnabled && Vector3.Distance(CurrentOutpost.transform.position, gameObject.transform.position) <= 1f)
        {
            _reached = true;
        }
        else
        {
            _reached = false;
        }
    }


    public Outpost GetValidOutpost()
    {
        Outpost v = Outpost.GetRandom();
        if (v == null) return null;
        if (v.CaptureValue < 1f || v.CurrentTeam != TeamNumber)
        {
            return v;
        }
        return null;
    }

    public void SetEnemyUnitAroundMe()
    {
        if (IsAlive == false) return;
        if (CurrentTarget != null && CurrentTarget.IsAlive) return;
        var aroundMe = Physics.OverlapSphere(transform.position, _enemyDetectionRadius, _unitLayerMask);
        foreach (var item in aroundMe)
        {
            if (item.transform == transform) continue; // Ignores Self

            var otherUnit = item.GetComponent<Unit>();
            if (otherUnit != null && otherUnit.IsAlive && otherUnit.TeamNumber != TeamNumber)
            {
                CurrentTarget = otherUnit;
                _anim.SetTrigger("attack");
                return;
            }
        }
        return;
    }

    override
    public void OnHit(float dmg)
    {
        if (IsAlive == false) return;

        base.OnHit(dmg);
        _healthImage.fillAmount = Mathf.InverseLerp(0, MaxHealth, _currentHealth);
    }

    override
    public void OnDie()
    {
        base.OnDie();
        _healthCanvas.gameObject.SetActive(false);
        GetComponent<PoolableObject>().DisablePoolableObject();
    }

}

