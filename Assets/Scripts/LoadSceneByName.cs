﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneByName : MonoBehaviour
{
    [SerializeField]
    private string _sceneName = "";

    [SerializeField]
    private bool _loadOnEnable = false;

    protected void OnEnable()
    {
        if (_loadOnEnable)
        {
            LoadSceneNow();
        }
    }

    public void LoadSceneNow()
    {
        SceneManager.LoadScene(_sceneName);
    }

}
