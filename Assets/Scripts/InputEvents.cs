﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class InputEvents : MonoBehaviour

{
    private CharacterController _charController;

    private void Awake()
    {
        _charController = GetComponent<CharacterController>();
    }

    public void Move(InputAction.CallbackContext context)
    {
        _charController.Move(context.ReadValue<Vector2>());
    }

    public void Jump()
    {
        _charController.Jump();
    }

    public void MouseInputs(InputAction.CallbackContext context)
    {
        _charController.MouseAxis(context.ReadValue<Vector2>());
    }

    public void Shoot(InputAction.CallbackContext context)
    {
        if (context.performed)
            _charController.Shooting();
    }

    public void CameraZoom(InputAction.CallbackContext context)
    {
        _charController.MouseAxis(context.ReadValue<Vector2>());
    }

    public void Sprint(InputAction.CallbackContext context)
    {
        _charController.Sprint(context.performed);
    }

}
