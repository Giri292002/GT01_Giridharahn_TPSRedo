using UnityEngine;
using PoolSystem;
public abstract class Unit : MonoBehaviour
{

    [Header("Unit")]
    public int TeamNumber = 0;

    [SerializeField]
    public float MaxHealth = 100f;
    protected float _currentHealth;

    [SerializeField]
    public float Damage = 8f;

    [SerializeField]
    protected Renderer _renderer;

    [SerializeField]
    protected PoolableObject _laserPrefab;

    protected Animator _anim;
    protected Eye[] _eyes;

    public bool IsAlive => _currentHealth > 0;

    abstract
    protected void UnitAwake();
    protected void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        _currentHealth = MaxHealth;
        UnitAwake();

        _eyes = GetComponentsInChildren<Eye>();

        Vector3 v3 = transform.position.With(y: 0f, x: 99f);
    }


    abstract
    protected void UnitStart();
    protected void Start()
    {
        SetTeam(TeamNumber);
        UnitStart();
    }

    public void SetTeam(int team)
    {
        TeamNumber = team;
        if (_renderer != null)
        {
            _renderer.material.color = GameManager.Instance.TeamColors[TeamNumber];
        }
    }

    /// <summary>
    /// Raycast from each eye to the pos, to see if any eye can see the other transform
    /// </summary>
    protected bool CanSee(Vector3 pos, Transform other)
    {
        foreach (var eye in _eyes)
        {
            var eyePos = eye.transform.position;
            var dir = pos - eyePos;
            if (Physics.Raycast(eyePos, dir, out var hit) && hit.transform == other)
            {
                return true;
            }
        }
        return false;
    }

    public void ShootLasersFromEyes(Vector3 pos, Unit target)
    {
        foreach (var eye in _eyes)
        {
            PoolManager.GetNext(_laserPrefab, eye.transform.position, Quaternion.identity).GetComponent<Laser>().Shoot(eye.transform.position, pos);
        }

        target?.OnHit(Damage);
    }


    virtual
    public void OnHit(float dmg)
    {
        if (IsAlive == false) return;
        _currentHealth -= dmg;
        if (_currentHealth <= 0f)
        {
            _currentHealth = 0f;
            OnDie();
        }
    }

    virtual
    public void OnDie()
    {
        _anim.SetBool("isDead", true);
    }

}

