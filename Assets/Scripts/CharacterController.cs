﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
public class CharacterController : Unit
{

    [Header("Player")]
    [SerializeField]
    private float _moveSpeed = 8f;

    [SerializeField]
    private float _sprintSpeedMult = 5f;

    [SerializeField]
    private float _jumpSpeed = 12f;

    [SerializeField]
    private Transform _camPivot;
    [SerializeField]
    private float _camsensitivity = 0.5f;
    [SerializeField]
    public int MaxShootCount = 10;
    [SerializeField]
    private float _coolDownSeconds = 2;
    [SerializeField]
    private float _noInputTime = 1f;

    private float _xInput;
    private float _yInput;
    private float _mouseX;
    private float _mouseY;
    private float _mouseScroll;
    private float _speedMult = 1f;
    private bool _jumpPressed;
    private Camera _camera;
    private Rigidbody _rigidBody;

    [SerializeField]
    private UnityEvent<int, int> _shootCooldownUIUpdate;
    [SerializeField]
    private UnityEvent<bool> _shootCooldownTextUpdate;
    private int _shootCount = 0;
    private bool _canShoot = true;

    [SerializeField]
    private UnityEvent<float> _healthUIUpdate;


    override
    protected void UnitAwake()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _camera = _camPivot.GetComponentInChildren<Camera>();
        Cursor.lockState = CursorLockMode.Locked;

    }

    override
    protected void UnitStart()
    {
        _shootCooldownUIUpdate.Invoke(_shootCount, MaxShootCount);
        _shootCooldownTextUpdate.Invoke(_canShoot);
    }


    private void Update()
    {
        ApplyCameraZoom();

        if (IsAlive == false) return;

        UpdateAnimations();
        ApplyRotations();
    }

    private void FixedUpdate()
    {
        if (IsAlive == false) return;

        ApplyPhysics();
    }

    public void Move(Vector2 _input)
    {
        _xInput = _input.x;
        _yInput = _input.y;
    }

    public void Jump()
    {
        _jumpPressed = true;
    }
    public void MouseAxis(Vector2 _input)
    {
        _mouseX = _input.x;
        _mouseY = _input.y;
    }

    public void CameraZoom(Vector2 _input)
    {
        _mouseScroll = _input.y;
    }

    public void Sprint(bool _heldSprint)
    {
        _speedMult = _heldSprint ? _sprintSpeedMult : 1f;
    }

    private void UpdateAnimations()
    {
        _anim.SetFloat("MoveX", _xInput);
        _anim.SetFloat("MoveZ", _yInput);
        _anim.SetFloat("Sprint", _speedMult);
    }

    private void ApplyRotations()
    {
        transform.Rotate(0f, _mouseX * _camsensitivity, 0f);
        _camPivot.Rotate(-_mouseY * _camsensitivity, 0f, 0f);
    }

    private void ApplyCameraZoom()
    {
        var camPos = _camera.transform.localPosition;
        camPos.z += _mouseScroll;
        camPos.z = Mathf.Clamp(camPos.z, -32f, 0f); // Hardcoded
        _camera.transform.localPosition = camPos;
    }

    private void ApplyPhysics()
    {
        // Movement Physics
        var moveVector = new Vector3(_xInput, 0f, _yInput) * _moveSpeed * _speedMult;
        moveVector = transform.TransformVector(moveVector); // Move according to local orientation

        // Jump
        if (_jumpPressed) _anim.SetTrigger("Jump");
        moveVector.y = _jumpPressed ? _jumpSpeed : _rigidBody.velocity.y;
        _jumpPressed = false; // Consumed Jump Button Press

        _rigidBody.velocity = moveVector;
    }

    public void Shooting()
    {
        if (Physics.Raycast(_camera.transform.position, _camera.transform.forward * 100f, out var hit))
        {
            if (CanSee(hit.point, hit.transform))
            {
                var otherUnit = hit.transform.GetComponent<Unit>();
                if (otherUnit != null && otherUnit.TeamNumber != TeamNumber && _canShoot) // Avoid Friendly fire
                {
                    StopCoroutine(checkForInput());
                    ShootLasersFromEyes(hit.point, otherUnit);
                    _shootCount++;
                    _shootCooldownUIUpdate.Invoke(_shootCount, MaxShootCount);
                    Invoke("StartCheckForInput", _noInputTime);

                    if (_shootCount >= MaxShootCount)
                    {
                        StartCoroutine(coolDown());
                        _canShoot = false;
                        _shootCooldownTextUpdate.Invoke(_canShoot);

                    }

                }
            }
        }
    }

    private void StartCheckForInput()
    {
        if (_canShoot)
        {
            StartCoroutine(checkForInput());
        }
    }
    private IEnumerator checkForInput()
    {
        while (_shootCount > 0 && _canShoot)
        {
            yield return new WaitForSeconds(1f);
            _shootCount--;
            _shootCooldownUIUpdate.Invoke(_shootCount, MaxShootCount);
        }
        yield break;
    }

    private IEnumerator coolDown()
    {
        yield return new WaitForSeconds(1f);

        float coolDownCycleSec = _coolDownSeconds / MaxShootCount;

        while (_shootCount > 0)
        {
            _shootCount--;
            _shootCooldownUIUpdate.Invoke(_shootCount, MaxShootCount);
            yield return new WaitForSeconds(coolDownCycleSec);
            if (_shootCount <= 0)
            {
                _canShoot = true;
                _shootCooldownTextUpdate.Invoke(_canShoot);
            }
        }


    }

    override
    public void OnHit(float dmg)
    {
        if (IsAlive == false) return;

        base.OnHit(dmg);
        _healthUIUpdate.Invoke(_currentHealth);

    }

    override
   public void OnDie()
    {
        base.OnDie();
    }


}
