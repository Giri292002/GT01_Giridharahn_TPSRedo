﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Projectile : MonoBehaviour
{
    [SerializeField]
    private float _projectileSpeed = 30f;

    private Rigidbody _rigidBody;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        _rigidBody.angularVelocity = Vector3.zero;
        _rigidBody.velocity = transform.forward * _projectileSpeed;
    }
}
