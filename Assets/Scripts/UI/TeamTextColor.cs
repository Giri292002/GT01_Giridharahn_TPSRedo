﻿using UnityEngine;
using TMPro;
using System.Collections;

[RequireComponent(typeof(TMP_Text))]
public class TeamTextColor : MonoBehaviour
{
    private TMP_Text _text;

    [SerializeField]
    private int _teamNumber = 0;

    protected IEnumerator Start()
    {
        while (GameManager.Instance == null)
        {
            yield return null;
        }

        _text = GetComponent<TMP_Text>();
        _text.color = GameManager.Instance.TeamColors[_teamNumber];
    }

    protected void Update()
    {
        if (GameManager.Instance == null || _text == null) return;

        _text.text = GameManager.Instance.TeamScores[_teamNumber].ToString();
    }

}
