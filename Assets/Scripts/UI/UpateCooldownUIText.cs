﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpateCooldownUIText : MonoBehaviour
{
    private Text _text;


    void Awake()
    {
        _text = gameObject.GetComponent<Text>();
    }

    public void UpdateCoolDownText(bool canShoot = true)
    {
        if (canShoot) _text.text = "";
        else _text.text = "WAIT TO COOLDOWN";
    }
}
