﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateHealth : MonoBehaviour
{
    private Image _healthImage;
    private Canvas _healthCanvas;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        _healthImage = gameObject.GetComponent<Image>();
        _healthCanvas = _healthImage.GetComponentInParent<Canvas>();
    }
    public void UpdateHealthUI(float health)
    {
        _healthImage.fillAmount = health / 100f;
        if (_healthImage.fillAmount <= 0) IsDead();
    }

    private void IsDead()
    {
        _healthCanvas.gameObject.SetActive(false);
    }
}
