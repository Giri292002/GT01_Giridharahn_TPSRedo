﻿using UnityEngine;

public class GameplayUIScreen : UIScreen
{
    protected void OnEnable()
    {
        Time.timeScale = 1f; // resume the game time
        Cursor.lockState = CursorLockMode.Locked;
    }

    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            UIScreenController.ShowScreen<PauseUIScreen>();
        }
    }
}
