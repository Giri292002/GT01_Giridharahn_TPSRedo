﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateShootCooldownUI : MonoBehaviour
{
    private Image _healthImage;
    private Canvas _healthCanvas;


    void Awake()
    {
        _healthImage = gameObject.GetComponent<Image>();
        _healthCanvas = _healthImage.GetComponentInParent<Canvas>();
    }


    public void UpdateCooldownUI(int shootCount, int maxShootCount)
    {
        Debug.Log(shootCount);
        _healthImage.fillAmount = Mathf.InverseLerp(0, maxShootCount, shootCount);
    }
}
