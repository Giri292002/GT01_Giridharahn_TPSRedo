﻿using PoolSystem;
using UnityEngine;

public class Tester : MonoBehaviour
{
    [SerializeField]
    private GameObject _notPoolableBulletPrefab;

    [SerializeField]
    private PoolableObject _poolableBulletPrefab;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            // Keep in mind, we cannot force it NOT to run
            System.GC.Collect(); // Force the GC to run
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            GameObject.Instantiate(_notPoolableBulletPrefab, Vector3.left, Quaternion.identity);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            PoolManager.GetNext(_poolableBulletPrefab, Vector3.right, Quaternion.identity);
        }
    }

}
