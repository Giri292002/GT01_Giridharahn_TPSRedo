﻿using UnityEngine;
using PoolSystem;

public class Laser : MonoBehaviour
{


    public void Shoot(Vector3 from, Vector3 to)
    {
        var line = GetComponent<LineRenderer>();
        line.SetPosition(0, from);
        line.SetPosition(1, to);
    }

    /// <summary>
    /// This function is called when the behaviour becomes disabled or inactive.
    /// </summary>
    void OnDisable()
    {
        var line = GetComponent<LineRenderer>();
    }
}
