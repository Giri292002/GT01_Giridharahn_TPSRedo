﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace PoolSystem
{
    public class PoolableObject : MonoBehaviour
    {
        public UnityEvent OnPoolableObjectDisable;

        [Tooltip("< 0 means infinite lifespan")]
        [SerializeField]
        private float _lifeSpan = -1f;

        private Coroutine _disabler;
        private WaitForSeconds _waitTime;

        private void Awake()
        {
            _waitTime = new WaitForSeconds(_lifeSpan);
        }

        private void OnEnable()
        {
            if (_lifeSpan >= 0f)
            {
                _disabler = StartCoroutine(Disabler());
            }
        }

        private void OnDisable()
        {
            ClearCoroutine();
        }

        private void ClearCoroutine()
        {
            if (_disabler == null) return;
            StopCoroutine(_disabler);
            _disabler = null;
        }

        private IEnumerator Disabler()
        {
            yield return _waitTime;
            DisablePoolableObject();
        }

        public void DisablePoolableObject()
        {
            ClearCoroutine();
            gameObject.SetActive(false);
            OnPoolableObjectDisable.Invoke();
        }

    }
}
