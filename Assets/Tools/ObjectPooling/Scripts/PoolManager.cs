﻿using System.Collections.Generic;
using UnityEngine;

namespace PoolSystem
{
    public static class PoolManager
    {
        private static readonly int _DEFAULT_POOL_SIZE = 5;
        private static readonly float _CHECK_PERCENT = 0.2f;

        // private static Dictionary<int, Queue<PoolableObject>> _pools = new Dictionary<int, Queue<PoolableObject>>();
        // private static Dictionary<int, Stack<PoolableObject>> _pools = new Dictionary<int, Stack<PoolableObject>>();
        private static Dictionary<int, List<PoolableObject>> _pools = new Dictionary<int, List<PoolableObject>>();
        private static Dictionary<int, int> _poolIndexes = new Dictionary<int, int>();


        public static void CreatePool(PoolableObject prefab, int poolSize)
        {
            int id = prefab.GetInstanceID();
            _pools[id] = new List<PoolableObject>(poolSize);
            _poolIndexes[id] = 0;

            for (int i = 0; i < poolSize; i++)
            {
                AddObjectToPool(id, prefab);
            }
        }

        private static PoolableObject AddObjectToPool(int poolId, PoolableObject prefab)
        {
            prefab.gameObject.SetActive(false);
            var clone = GameObject.Instantiate(prefab);
            _pools[poolId].Add(clone);
            return clone;
        }

        public static PoolableObject GetNext(PoolableObject prefab)
        {
            int poolId = prefab.GetInstanceID();

            if (_pools.ContainsKey(poolId) == false)
            {
                CreatePool(prefab, _DEFAULT_POOL_SIZE);
            }

            var poolList = _pools[poolId];
            var checkCount = _CHECK_PERCENT * poolList.Count;
            for (int i = 0; i < checkCount; i++)
            {
                _poolIndexes[poolId] = (int)((_poolIndexes[poolId] + 1) % checkCount);
                int index = _poolIndexes[poolId];
                var poolItem = poolList[index];
                if (poolItem.gameObject.activeInHierarchy == false)
                {
                    return poolItem;
                }
            }

            return AddObjectToPool(poolId, prefab);
        }

        public static PoolableObject GetNext(PoolableObject prefab, Vector3 pos, Quaternion rot, bool active = true)
        {
            var clone = GetNext(prefab);
            clone.transform.position = pos;
            clone.transform.rotation = rot;
            clone.gameObject.SetActive(active);
            return clone;
        }

    }
}
